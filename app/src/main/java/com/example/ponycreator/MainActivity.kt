package com.example.ponycreator

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.rounded.Check
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.ponycreator.ui.theme.PonyCreatorTheme

class MainActivity : ComponentActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContent {
			PonyCreatorTheme {
				Surface(
					modifier = Modifier.fillMaxSize(),
					color = MaterialTheme.colors.background
				) {
					PonyCreatorApp()
				}
			}
		}
	}
}

@Composable
fun PonyCreatorApp() {

	val horn = remember { mutableStateOf(false) }
	val wings = remember { mutableStateOf(false) }

	val ponyColor = remember { mutableStateOf(Color.Magenta) }
	val maneColor = remember { mutableStateOf(Color.Magenta) }
	val eyeColor = remember { mutableStateOf(Color.Magenta) }

	val mainColors = listOf(
		Color(0xFFFFC5FE), Color(0xFFFEE17D), Color(0xFFF8FFB1),
		Color(0xFFC9FFAB), Color(0xFF89FFCD), Color(0xFF69F2FE),
		Color(0xFF9DCBFC), Color(0xFFD4B2FE), Color(0xFFFAB8C9),
	)
	val altColors = listOf(
		Color(0xFFFF7CFF), Color(0xFFFFC562), Color(0xFFF7FE4C),
		Color(0xFF8DFF6B), Color(0xFF2AE99C), Color(0xFF08DEFF),
		Color(0xFF9B78FC), Color(0xFFD454FE), Color(0xFFFD78C5),
	)
	Card(
		modifier = Modifier
			.fillMaxSize()
			.padding(26.dp),
		elevation = 9.dp,
		shape = RoundedCornerShape(7),
		backgroundColor = Color(0xFFEDEDED)
	) {
		Column(horizontalAlignment = Alignment.CenterHorizontally) {
			Text(
				text = "Создай свою пони",
				style = MaterialTheme.typography.h5,
				modifier = Modifier.padding(16.dp)
			)
			ColorPicker(
				"цвет пони",
				mainColors,
				ponyColor,
			)
			ColorPicker(
				"цвет гривы",
				altColors,
				maneColor
			)
			ColorPicker(
				"цвет глаз",
				mainColors,
				eyeColor
			)
			Row(
				modifier = Modifier
					.fillMaxWidth()
					.padding(20.dp),
				verticalAlignment = Alignment.CenterVertically,
				horizontalArrangement = Arrangement.SpaceEvenly
			) {
				MyCheckbox(checked = horn, text = "наличие рога")
				Spacer(Modifier.width(10.dp))
				MyCheckbox(checked = wings, text = "наличие крыльев")
			}
			OutlinedButton(
				border = BorderStroke(2.dp, Color(0xff6e6e6e)),
				shape = RoundedCornerShape(30),
				colors = ButtonDefaults.buttonColors(Color(0xFFEDEDED)),

				onClick = { /*TODO*/ }
			) {
				Text(
					color = Color(0xff6e6e6e),
					text = "Создать"
				)
			}
			Card(
				modifier = Modifier.size(260.dp, 160.dp),
				backgroundColor = Color(0xFFEDEDED),
				shape = RoundedCornerShape(10),
				border = BorderStroke(2.dp, Color(0xff6e6e6e)),
			) {
				Column(
					modifier = Modifier
						.fillMaxSize()
						.align(CenterHorizontally),
					verticalArrangement = Arrangement.Center
				) {
					Text(
						modifier = Modifier.align(CenterHorizontally),
						color = Color(0xff6e6e6e),
						text = "результат"
					)
					Text(
						modifier = Modifier.align(CenterHorizontally),
						color = ponyColor.value,
						text = "цвет пони"
					)
					Text(
						modifier = Modifier.align(CenterHorizontally),
						color = maneColor.value,
						text = "цвет гривы"
					)
					Text(
						modifier = Modifier.align(CenterHorizontally),
						color = eyeColor.value,
						text = "цвет глаз"
					)
				}
			}
		}
	}
}

@Composable
fun ColorPicker(text: String, colorList: List<Color>, color: MutableState<Color>) {
	Text(
		text = text,
		modifier = Modifier.padding(6.dp),
		fontSize = 20.sp,
		color = Color(0xff6e6e6e)
	)
	Row(
		modifier = Modifier.size(width = 280.dp, height = 35.dp),
		horizontalArrangement = Arrangement.SpaceBetween
	) {
		repeat(9) {
			OutlinedButton(
				onClick = {
					color.value = colorList[it]
				},
				border = BorderStroke(2.dp, Color.White),
				shape = RoundedCornerShape(30),
				modifier = Modifier
					.height(30.dp)
					.width(30.dp),
				colors = ButtonDefaults.buttonColors(backgroundColor = colorList[it])
			) {}
		}
	}
}

@Composable
fun MyCheckbox(checked: MutableState<Boolean>, text: String = "") {
	Card(
		elevation = 0.dp,
		shape = RoundedCornerShape(30),
		border = BorderStroke(1.5.dp, color = Color(0xff6e6e6e))
	) {
		Box(
			modifier = Modifier
				.size(20.dp)
				.background(Color(0xFFDADADA))
				.clickable { checked.value = !checked.value },

			) {
			if (checked.value) Icon(
				imageVector = Icons.Rounded.Check,
				contentDescription = "",
				tint = Color(0xFF6D6D6D)
			)
		}
	}
	Text(
		text = text,
		color = Color(0xff6e6e6e),
	)
}


@Preview(showBackground = true, showSystemUi = true)
@Composable
fun DefaultPreview() {
	PonyCreatorTheme {
		PonyCreatorApp()
	}
}